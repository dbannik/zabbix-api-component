<?php

declare(strict_types=1);

namespace Zabbix\SessionStorage;

use Zabbix\Model\Subject\SessionIdentification;

interface SessionStorageInterface
{
    public function getSession(): ?SessionIdentification;

    public function setSession(?SessionIdentification $session): void;
}
