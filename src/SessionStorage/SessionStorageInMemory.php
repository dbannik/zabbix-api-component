<?php

declare(strict_types=1);

namespace Zabbix\SessionStorage;

use Zabbix\Model\Subject\SessionIdentification;

final class SessionStorageInMemory implements SessionStorageInterface
{
    private ?SessionIdentification $session = null;

    public function getSession(): ?SessionIdentification
    {
        return $this->session;
    }

    public function setSession(?SessionIdentification $session): void
    {
        $this->session = $session;
    }
}
