<?php

declare(strict_types=1);

namespace Zabbix\SessionStorage;

use Zabbix\Model\Subject\SessionIdentification;

final class SessionStorageInFile implements SessionStorageInterface
{
    private string $file;

    private ?SessionIdentification $session = null;

    public function __construct(string $file)
    {
        if (\is_file($file)) {
            $data          = \file_get_contents($file);
            $this->session = \unserialize((string) $data, []);
        }

        $this->file = $file;
    }

    public function getSession(): ?SessionIdentification
    {
        return $this->session;
    }

    public function setSession(?SessionIdentification $session): void
    {
        $this->session = $session;
    }

    public function __destruct()
    {
        $data = \serialize($this->session);
        \file_put_contents($this->file, $data);
    }
}
