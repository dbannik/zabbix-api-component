<?php

declare(strict_types=1);

namespace Zabbix\Exception;

use Exception;

class ZabbixException extends Exception
{
}
