<?php

declare(strict_types=1);

namespace Zabbix\Exception;

class UnauthorizedException extends ZabbixException
{
}
