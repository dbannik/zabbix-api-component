<?php

declare(strict_types=1);

namespace Zabbix\Exception;

class RuntimeException extends ZabbixException
{
}
