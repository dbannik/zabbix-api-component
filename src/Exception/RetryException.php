<?php

declare(strict_types=1);

namespace Zabbix\Exception;

class RetryException extends ZabbixException
{
}
