<?php

declare(strict_types=1);

namespace Zabbix\Exception;

class ResponseException extends ZabbixException
{
}
