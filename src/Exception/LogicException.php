<?php

declare(strict_types=1);

namespace Zabbix\Exception;

class LogicException extends ZabbixException
{
}
