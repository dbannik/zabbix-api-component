<?php

declare(strict_types=1);

namespace Zabbix\Serializer;

use Symfony\Component\Serializer\SerializerInterface;
use Webmozart\Assert\Assert;
use Zabbix\ApiConnection\Request;
use Zabbix\ApiConnection\Response;
use Zabbix\Model\AbstractResponse;

final class ModelSerializer
{
    private SerializerInterface $serializer;

    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    public function serialize(string $responseModal, Response $response): object
    {
        Assert::subclassOf($responseModal, AbstractResponse::class);
        $contentType = $response->getHeader('Content-Type')[0];
        Assert::eq(Request::CONTENT_TYPE_JSON, $contentType);

        $result = $this->serializer->deserialize($response->getBody(), $responseModal, 'json');
        Assert::object($result);

        return $result;
    }
}
