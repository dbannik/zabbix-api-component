<?php

declare(strict_types=1);

namespace Zabbix\Serializer;

use Symfony\Component\Serializer\SerializerInterface;
use Zabbix\ApiConnection\Request;
use Zabbix\Model\AbstractRequest;

final class RequestSerializer
{
    private SerializerInterface $serializer;

    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    public function serialize(string $url, AbstractRequest $model): Request
    {
        $body = $this->serializer->serialize($model, 'json');

        return new Request($url, Request::METHOD_POST, Request::CONTENT_TYPE_JSON, $body);
    }
}
