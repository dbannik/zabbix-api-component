<?php

declare(strict_types=1);

namespace Zabbix\Message;

use Zabbix\Model\AbstractRequest;

interface MessageInterface
{
    public function createRequest(): AbstractRequest;

    public function getResponseClass(): string;
}
