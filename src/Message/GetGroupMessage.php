<?php

declare(strict_types=1);

namespace Zabbix\Message;

use Zabbix\Model\AbstractRequest;
use Zabbix\Model\HostGroupGetRequest;
use Zabbix\Model\HostGroupsResponse;

final class GetGroupMessage implements MessageInterface
{
    /**
     * @var string[]
     */
    public array $names;

    public function createRequest(): AbstractRequest
    {
        return new HostGroupGetRequest($this->names);
    }

    public function getResponseClass(): string
    {
        return HostGroupsResponse::class;
    }
}
