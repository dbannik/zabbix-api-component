<?php

declare(strict_types=1);

namespace Zabbix\Message;

use Zabbix\Model\AbstractRequest;
use Zabbix\Model\HostCreateRequest;
use Zabbix\Model\HostIdsResponse;
use Zabbix\Model\Subject\Group;
use Zabbix\Model\Subject\InterfaceModel;
use Zabbix\Model\Subject\Template;

class AddHostMessage implements MessageInterface
{
    public InterfaceModel $interface;

    public Group $group;

    public Template $template;

    public string $name;

    public function __construct(string $name)
    {
        $this->interface = new InterfaceModel();
        $this->group     = new Group();
        $this->template  = new Template();
        $this->name      = $name;
    }

    public function createRequest(): AbstractRequest
    {
        $model = new HostCreateRequest();
        $model->setHost($this->name);
        $model->setInterfaces([$this->interface]);
        $model->setGroups([$this->group]);
        $model->setTemplates([$this->template]);

        return $model;
    }

    public function getResponseClass(): string
    {
        return HostIdsResponse::class;
    }
}
