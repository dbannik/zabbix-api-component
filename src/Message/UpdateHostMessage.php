<?php

declare(strict_types=1);

namespace Zabbix\Message;

use Zabbix\Model\AbstractRequest;
use Zabbix\Model\HostIdsResponse;
use Zabbix\Model\HostUpdateRequest;

class UpdateHostMessage implements MessageInterface
{
    public string $hostId;

    public int $status;

    public function createRequest(): AbstractRequest
    {
        return new HostUpdateRequest($this->hostId, $this->status);
    }

    public function getResponseClass(): string
    {
        return HostIdsResponse::class;
    }
}
