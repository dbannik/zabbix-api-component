<?php

declare(strict_types=1);

namespace Zabbix\Message;

use Zabbix\Model\AbstractRequest;
use Zabbix\Model\HostDeleteRequest;
use Zabbix\Model\HostIdsResponse;

final class DeleteHostMessage implements MessageInterface
{
    /**
     * @var string[]
     */
    public array $hostIds;

    public function createRequest(): AbstractRequest
    {
        return new HostDeleteRequest($this->hostIds);
    }

    public function getResponseClass(): string
    {
        return HostIdsResponse::class;
    }
}
