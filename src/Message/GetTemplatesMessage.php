<?php

declare(strict_types=1);

namespace Zabbix\Message;

use Zabbix\Model\AbstractRequest;
use Zabbix\Model\TemplateGetRequest;
use Zabbix\Model\TemplateResponse;

final class GetTemplatesMessage implements MessageInterface
{
    /**
     * @var string[]
     */
    public array $names;

    public function createRequest(): AbstractRequest
    {
        return new TemplateGetRequest($this->names);
    }

    public function getResponseClass(): string
    {
        return TemplateResponse::class;
    }
}
