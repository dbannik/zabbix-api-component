<?php

declare(strict_types=1);

namespace Zabbix\Message;

use Zabbix\Model\AbstractRequest;
use Zabbix\Model\HostGetRequest;
use Zabbix\Model\HostsResponse;

final class GetHostsMessage implements MessageInterface
{
    /**
     * @var null|string[]
     */
    public ?array $hostsIds = null;

    /**
     * @var null|string[]
     */
    public ?array $templateIds = null;

    /**
     * @var null|string[]
     */
    public ?array $groupIds = null;

    /**
     * @var null|string[]
     */
    public ?array $hosts = null;

    public function createRequest(): AbstractRequest
    {
        $hosts = $this->hosts;
        $model = new HostGetRequest($this->hostsIds, $this->groupIds, $this->templateIds);
        if (\is_array($hosts)) {
            $model->params['filter']->host = $hosts;
        }

        return $model;
    }

    public function getResponseClass(): string
    {
        return HostsResponse::class;
    }
}
