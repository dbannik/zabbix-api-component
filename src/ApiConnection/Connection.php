<?php

declare(strict_types=1);

namespace Zabbix\ApiConnection;

use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\RequestOptions;
use Zabbix\Exception\ResponseException;

final class Connection
{
    private ClientInterface $client;

    public function __construct(ClientInterface $client)
    {
        $this->client = $client;
    }

    public function send(Request $request): Response
    {
        $options = [
            RequestOptions::BODY    => $request->getBody(),
            RequestOptions::HEADERS => [
                'Content-Type' => $request->getContentType(),
            ],
        ];

        try {
            $response = $this->client->request($request->getMethod(), $request->getUrl(), $options);
            $content  = $response->getBody()->getContents();

            return new Response($response->getStatusCode(), $response->getHeaders(), $content);
        } catch (GuzzleException $requestException) {
            throw new ResponseException('Failed to process request', 0, $requestException);
        }
    }
}
