<?php

declare(strict_types=1);

namespace Zabbix\ApiConnection;

use Webmozart\Assert\Assert;

final class Request
{
    public const METHOD_GET = 'GET';

    public const METHOD_POST = 'POST';

    public const METHODS = [
        self::METHOD_GET,
        self::METHOD_POST,
    ];

    public const CONTENT_TYPE_JSON = 'application/json';

    private string $method;

    private string $contentType;

    private string $body;

    private string $url;

    /**
     * @throws \JsonException
     */
    public function __construct(string $url, string $method, string $contentType, string $body)
    {
        Assert::oneOf($method, self::METHODS);
        Assert::eq($contentType, self::CONTENT_TYPE_JSON);
        Assert::notEmpty(\json_decode($body, true, 512, JSON_THROW_ON_ERROR));

        $this->method      = $method;
        $this->contentType = $contentType;
        $this->body        = $body;
        $this->url         = $url;
    }

    public function getMethod(): string
    {
        return $this->method;
    }

    public function getContentType(): string
    {
        return $this->contentType;
    }

    public function getBody(): string
    {
        return $this->body;
    }

    public function getUrl(): string
    {
        return $this->url;
    }
}
