<?php

declare(strict_types=1);

namespace Zabbix\ApiConnection;

final class Response
{
    private int $code;

    private string $body;

    /**
     * @var array<array>
     */
    private array $headers;

    /**
     * @param int          $code
     * @param array<array> $headers
     * @param string       $body
     */
    public function __construct(int $code, array $headers, string $body)
    {
        $this->code    = $code;
        $this->body    = $body;
        $this->headers = $headers;
    }

    public function getCode(): int
    {
        return $this->code;
    }

    public function getBody(): string
    {
        return $this->body;
    }

    /**
     * @return array[]
     */
    public function getHeaders(): array
    {
        return $this->headers;
    }

    /**
     * @param string $name
     *
     * @return string[]
     */
    public function getHeader(string $name): array
    {
        return $this->headers[$name] ?? [];
    }
}
