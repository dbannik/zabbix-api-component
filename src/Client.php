<?php

declare(strict_types=1);

namespace Zabbix;

use Traversable;
use Zabbix\Exception\RuntimeException;
use Zabbix\Message\MessageInterface;
use Zabbix\Middleware\MiddlewareInterface;
use Zabbix\Model\AbstractResponse;
use Zabbix\SessionStorage\SessionStorageInterface;

class Client
{
    /** @var MiddlewareInterface[]|Traversable */
    private Traversable $middlewares;

    private SessionStorageInterface $storage;

    /**
     * @param MiddlewareInterface[]|Traversable $middlewares
     */
    public function __construct(Traversable $middlewares, SessionStorageInterface $storage)
    {
        $this->middlewares = $middlewares;
        $this->storage     = $storage;
    }

    public function handle(MessageInterface $message): AbstractResponse
    {
        $request       = $message->createRequest();
        $responseClass = $message->getResponseClass();
        $envelope      = new Envelope($request, $responseClass, $this->storage);

        $this->executionChain($envelope);

        if (null === $envelope->modelResponse) {
            throw new RuntimeException(\sprintf('%s::modelResponse should have returned an object %s', Envelope::class, AbstractResponse::class));
        }

        return $envelope->modelResponse;
    }

    private function executionChain(Envelope $envelope): Envelope
    {
        $lastCallable   = static fn (Envelope $envelope) => $envelope;
        $middlewareList = \iterator_to_array($this->middlewares);

        while ($middleware = \array_pop($middlewareList)) {
            $lastCallable = static fn (Envelope $envelope) => $middleware->execute($envelope, $lastCallable);
        }

        return $lastCallable($envelope);
    }
}
