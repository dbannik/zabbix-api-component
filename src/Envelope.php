<?php

declare(strict_types=1);

namespace Zabbix;

use Zabbix\Model\AbstractRequest;
use Zabbix\Model\AbstractResponse;
use Zabbix\SessionStorage\SessionStorageInterface;

final class Envelope
{
    private AbstractRequest $modelRequest;

    private string $responseModalClass;

    private SessionStorageInterface $storage;

    public ?AbstractResponse $modelResponse = null;

    public function __construct(AbstractRequest $modelRequest, string $responseClass, SessionStorageInterface $storage)
    {
        $this->modelRequest       = $modelRequest;
        $this->responseModalClass = $responseClass;
        $this->storage            = $storage;
    }

    public function getModelRequest(): AbstractRequest
    {
        return $this->modelRequest;
    }

    public function getResponseModalClass(): string
    {
        return $this->responseModalClass;
    }

    public function getStorage(): SessionStorageInterface
    {
        return $this->storage;
    }
}
