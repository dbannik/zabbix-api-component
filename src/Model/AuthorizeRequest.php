<?php

declare(strict_types=1);

namespace Zabbix\Model;

class AuthorizeRequest extends AbstractRequest
{
    public string $method = 'user.login';

    public function __construct(string $user, string $password)
    {
        $this->params = [
            'user'     => $user,
            'password' => $password,
        ];
    }
}
