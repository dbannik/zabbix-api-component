<?php

declare(strict_types=1);

namespace Zabbix\Model\Subject;

class HostParamFilter
{
    /**
     * @var string[]
     */
    public array $host = [];
}
