<?php

declare(strict_types=1);

namespace Zabbix\Model\Subject;

final class HostGroup
{
    public string $groupid;

    public string $name;

    public string $flag;

    public string $internal;
}
