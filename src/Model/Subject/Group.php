<?php

declare(strict_types=1);

namespace Zabbix\Model\Subject;

class Group
{
    public ?string $groupid;
}
