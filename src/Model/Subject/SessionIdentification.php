<?php

declare(strict_types=1);

namespace Zabbix\Model\Subject;

class SessionIdentification
{
    private int $id;

    private string $token;

    public function __construct(int $id, string $token)
    {
        $this->id    = $id;
        $this->token = $token;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getToken(): string
    {
        return $this->token;
    }
}
