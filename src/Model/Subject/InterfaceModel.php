<?php

declare(strict_types=1);

namespace Zabbix\Model\Subject;

class InterfaceModel
{
    public ?string $type;

    public ?string $main;

    public ?string $useip;

    public ?string $ip;

    public ?string $dns;

    public ?string $port;
}
