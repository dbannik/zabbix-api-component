<?php

declare(strict_types=1);

namespace Zabbix\Model\Subject;

class Error
{
    public const ALREADY_EXISTS = -32602;

    public const CODES = [
        self::ALREADY_EXISTS,
    ];

    public ?int $code;

    public ?string $message;

    public ?string $data;

    public function isAlreadyExists(): bool
    {
        return self::ALREADY_EXISTS === $this->code;
    }

    public function __toString(): string
    {
        return \sprintf('[%s] %s %s', $this->code, $this->message, $this->data);
    }
}
