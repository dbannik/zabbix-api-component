<?php

declare(strict_types=1);

namespace Zabbix\Model\Subject;

/**
 * @SuppressWarnings("CamelCasePropertyName")
 */
class Host
{
    public string $hostid;

    public string $proxy_hostid;

    public string $host;

    public string $status;

    public string $disable_until;

    public string $error;

    public string $available;

    public string $errors_from;

    public string $lastaccess;

    public string $ipmi_authtype;

    public string $ipmi_privilege;

    public string $ipmi_username;

    public string $ipmi_password;

    public string $ipmi_disable_until;

    public string $ipmi_available;

    public string $snmp_disable_until;

    public string $snmp_available;

    public string $maintenanceid;

    public string $maintenance_status;

    public string $maintenance_type;

    public string $maintenance_from;

    public string $ipmi_errors_from;

    public string $snmp_errors_from;

    public string $ipmi_error;

    public string $snmp_error;

    public string $jmx_disable_until;

    public string $jmx_available;

    public string $jmx_errors_from;

    public string $jmx_error;

    public string $name;

    public string $flags;

    public string $templateid;

    public string $description;

    public string $tls_connect;

    public string $tls_accept;

    public string $tls_issuer;

    public string $tls_subject;

    public string $tls_psk_identity;

    public string $tls_psk;

    public string $proxy_address;

    public string $auto_compress;

    public string $inventory_mode;
}
