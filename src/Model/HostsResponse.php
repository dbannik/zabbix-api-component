<?php

declare(strict_types=1);

namespace Zabbix\Model;

use Webmozart\Assert\Assert;
use Zabbix\Model\Subject\Host;

class HostsResponse extends AbstractResponse
{
    /**
     * @var Host[]
     */
    public $result = [];

    /**
     * @param Host[] $hosts
     */
    public function setResult(array $hosts): void
    {
        Assert::allIsInstanceOf($hosts, Host::class);
        $this->result = $hosts;
    }

    /**
     * @return Host[]
     */
    public function getResult(): array
    {
        return $this->result;
    }
}
