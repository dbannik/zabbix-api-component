<?php

declare(strict_types=1);

namespace Zabbix\Model;

final class HostGroupGetRequest extends AbstractRequest
{
    public string $method = 'hostgroup.get';

    public array $params;

    /**
     * @param string[] $names
     */
    public function __construct(array $names)
    {
        $this->params = [
            'filter' => [
                'name' => $names,
            ],
        ];
    }
}
