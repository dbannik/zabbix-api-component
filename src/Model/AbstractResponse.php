<?php

declare(strict_types=1);

namespace Zabbix\Model;

use Zabbix\Model\Subject\Error;

abstract class AbstractResponse
{
    /**
     * @var null|Error
     */
    public ?Error $error = null;

    /**
     * @var mixed
     */
    public $result;

    /**
     * @param null|Error $error
     */
    public function setError(?Error $error): void
    {
        $this->error = $error;
    }

    public function isError(): bool
    {
        return null !== $this->error;
    }
}
