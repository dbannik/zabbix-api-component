<?php

declare(strict_types=1);

namespace Zabbix\Model;

class AuthResponse extends AbstractResponse
{
    public int $id;

    /**
     * @var string
     */
    public $result;

    public function getId(): int
    {
        return $this->id;
    }

    public function getResult(): string
    {
        return $this->result;
    }
}
