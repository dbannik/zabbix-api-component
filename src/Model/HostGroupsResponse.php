<?php

declare(strict_types=1);

namespace Zabbix\Model;

use Webmozart\Assert\Assert;
use Zabbix\Model\Subject\HostGroup;

final class HostGroupsResponse extends AbstractResponse
{
    /**
     * @var HostGroup[]
     */
    public $result;

    /**
     * @return HostGroup[]
     */
    public function getResult(): array
    {
        return $this->result;
    }

    /**
     * @param HostGroup[] $result
     */
    public function setResult(array $result): void
    {
        Assert::allIsInstanceOf($result, HostGroup::class);
        $this->result = $result;
    }
}
