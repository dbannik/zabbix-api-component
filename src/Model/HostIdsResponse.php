<?php

declare(strict_types=1);

namespace Zabbix\Model;

class HostIdsResponse extends AbstractResponse
{
    /**
     * @var array[]
     */
    public $result = [];

    /**
     * @return string[]
     */
    public function getHostsIds(): array
    {
        return $this->result['hostids'] ?? [];
    }
}
