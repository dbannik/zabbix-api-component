<?php

declare(strict_types=1);

namespace Zabbix\Model;

use Webmozart\Assert\Assert;
use Zabbix\Model\Subject\Template;

final class TemplateResponse extends AbstractResponse
{
    /**
     * @var Template[]
     */
    public $result;

    /**
     * @return Template[]
     */
    public function getResult(): array
    {
        return $this->result;
    }

    /**
     * @param Template[] $result
     */
    public function setResult(array $result): void
    {
        Assert::allIsInstanceOf($result, Template::class);
        $this->result = $result;
    }
}
