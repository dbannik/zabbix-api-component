<?php

declare(strict_types=1);

namespace Zabbix\Model;

use Zabbix\Model\Subject\SessionIdentification;

abstract class AbstractRequest
{
    public string $jsonrpc = '2.0';

    public string $method;

    /**
     * @var array<mixed>
     */
    public array $params = [];

    public int $id = 1;

    public ?string $auth = null;

    public function setSession(SessionIdentification $session): void
    {
        $this->id   = $session->getId();
        $this->auth = $session->getToken();
    }
}
