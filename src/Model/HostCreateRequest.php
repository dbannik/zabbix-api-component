<?php

declare(strict_types=1);

namespace Zabbix\Model;

use Webmozart\Assert\Assert;
use Zabbix\Model\Subject\Group;
use Zabbix\Model\Subject\InterfaceModel;
use Zabbix\Model\Subject\Template;

class HostCreateRequest extends AbstractRequest
{
    public string $method = 'host.create';

    /**
     * @param Template[] $templates
     *
     * @return $this
     */
    public function setTemplates(array $templates): self
    {
        Assert::allIsInstanceOf($templates, Template::class);
        $this->params['templates'] = $templates;

        return $this;
    }

    /**
     * @param Group[] $groups
     *
     * @return $this
     */
    public function setGroups(array $groups): self
    {
        Assert::allIsInstanceOf($groups, Group::class);
        $this->params['groups'] = $groups;

        return $this;
    }

    /**
     * @param InterfaceModel[] $interfaces
     *
     * @return $this
     */
    public function setInterfaces(array $interfaces): self
    {
        Assert::allIsInstanceOf($interfaces, InterfaceModel::class);
        $this->params['interfaces'] = $interfaces;

        return $this;
    }

    public function setHost(string $host): self
    {
        $this->params['host'] = $host;

        return $this;
    }
}
