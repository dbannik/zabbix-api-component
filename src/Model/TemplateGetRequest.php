<?php

declare(strict_types=1);

namespace Zabbix\Model;

final class TemplateGetRequest extends AbstractRequest
{
    public string $method = 'template.get';

    public array $params;

    /**
     * @param string[] $hosts
     */
    public function __construct(array $hosts)
    {
        $this->params = [
            'filter' => [
                'host' => $hosts,
            ],
        ];
    }
}
