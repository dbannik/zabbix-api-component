<?php

declare(strict_types=1);

namespace Zabbix\Model;

use Zabbix\Model\Subject\HostParamFilter;

class HostGetRequest extends AbstractRequest
{
    public string $method = 'host.get';

    /**
     * @var array{hostids: array|null, groupids: array|null, templateids: array|null, filter: HostParamFilter}
     */
    public array $params;

    /**
     * @param null|string[] $hostIds
     * @param null|string[] $groupIds
     * @param null|string[] $templateIds
     */
    public function __construct(?array $hostIds = null, ?array $groupIds = null, ?array $templateIds = null)
    {
        $this->params = [
            'hostids'     => $hostIds,
            'groupids'    => $groupIds,
            'templateids' => $templateIds,
            'filter'      => new HostParamFilter(),
        ];
    }

    /**
     * @param string[] $hostsIds
     *
     * @return $this
     */
    public function setHosts(array $hostsIds): self
    {
        $this->params['hostids'] = $hostsIds;

        return $this;
    }

    /**
     * @param string[] $groupIds
     *
     * @return $this
     */
    public function setGroups(array $groupIds): self
    {
        $this->params['groupids'] = $groupIds;

        return $this;
    }

    /**
     * @param string[] $templateIds
     *
     * @return $this
     */
    public function setTemplates(array $templateIds): self
    {
        $this->params['templateids'] = $templateIds;

        return $this;
    }
}
