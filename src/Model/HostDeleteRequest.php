<?php

declare(strict_types=1);

namespace Zabbix\Model;

class HostDeleteRequest extends AbstractRequest
{
    public string $method = 'host.delete';

    /**
     * @var string[]
     */
    public array $params;

    /**
     * @param string[] $hostIds
     */
    public function __construct(array $hostIds)
    {
        $this->params = $hostIds;
    }
}
