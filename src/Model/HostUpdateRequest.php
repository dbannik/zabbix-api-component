<?php

declare(strict_types=1);

namespace Zabbix\Model;

class HostUpdateRequest extends AbstractRequest
{
    public string $method = 'host.update';

    public function __construct(string $hostId, int $status)
    {
        $this->params = [
            'hostid' => $hostId,
            'status' => $status,
        ];
    }
}
