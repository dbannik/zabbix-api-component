<?php

declare(strict_types=1);

namespace Zabbix\Handler;

use Zabbix\ApiConnection\Connection;
use Zabbix\Exception\LogicException;
use Zabbix\Model\AbstractRequest;
use Zabbix\Model\AbstractResponse;
use Zabbix\Serializer\ModelSerializer;
use Zabbix\Serializer\RequestSerializer;

final class RequestHandler
{
    private string $host;

    private RequestSerializer $requestSerializer;

    private Connection $connection;

    private ModelSerializer $modelSerializer;

    public function __construct(
        Connection $connection,
        RequestSerializer $requestSerializer,
        ModelSerializer $modelSerializer,
        string $host
    ) {
        $this->connection        = $connection;
        $this->requestSerializer = $requestSerializer;
        $this->modelSerializer   = $modelSerializer;
        $this->host              = \sprintf('%s/api_jsonrpc.php', $host);
    }

    public function __invoke(AbstractRequest $model, string $responseClass): AbstractResponse
    {
        $request   = $this->requestSerializer->serialize($this->host, $model);
        $response  = $this->connection->send($request);
        $respModel =  $this->modelSerializer->serialize($responseClass, $response);

        if (!$respModel instanceof AbstractResponse) {
            throw new LogicException('');
        }

        return $respModel;
    }
}
