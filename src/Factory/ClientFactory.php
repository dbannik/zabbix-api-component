<?php

declare(strict_types=1);

namespace Zabbix\Factory;

use ArrayIterator;
use GuzzleHttp\Client as HttpClient;
use Zabbix\ApiConnection\Connection;
use Zabbix\Client;
use Zabbix\Credentials;
use Zabbix\Handler\RequestHandler;
use Zabbix\Middleware\AuthorizeMiddleware;
use Zabbix\Middleware\HandleMiddleware;
use Zabbix\Middleware\RetryAuthorizeMiddleware;
use Zabbix\Serializer\ModelSerializer;
use Zabbix\Serializer\RequestSerializer;
use Zabbix\SessionStorage\SessionStorageInMemory;

/**
 * @SuppressWarnings("CouplingBetweenObjects")
 */
final class ClientFactory
{
    public static function create(string $host, Credentials $credentials): Client
    {
        $connection        = new Connection(new HttpClient());
        $serializer        = (new FactorySerializer())->create();
        $requestSerializer = new RequestSerializer($serializer);
        $modelSerializer   = new ModelSerializer($serializer);
        $handler           = new RequestHandler($connection, $requestSerializer, $modelSerializer, $host);
        $sessionStorage    = new SessionStorageInMemory();
        $iterator          = new ArrayIterator([
            new RetryAuthorizeMiddleware(3),
            new AuthorizeMiddleware($credentials, $handler),
            new HandleMiddleware($handler),
        ]);

        return new Client($iterator, $sessionStorage);
    }
}
