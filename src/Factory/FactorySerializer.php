<?php

declare(strict_types=1);

namespace Zabbix\Factory;

use Symfony\Component\PropertyInfo\Extractor\PhpDocExtractor;
use Symfony\Component\PropertyInfo\Extractor\ReflectionExtractor;
use Symfony\Component\PropertyInfo\PropertyInfoExtractor;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;

final class FactorySerializer
{
    public function create(): SerializerInterface
    {
        $reflectionExtractor   = new ReflectionExtractor();
        $phpDocExtractor       = new PhpDocExtractor();
        $propertyTypeExtractor = new PropertyInfoExtractor([$reflectionExtractor], [$phpDocExtractor, $reflectionExtractor], [$phpDocExtractor], [$reflectionExtractor], [$reflectionExtractor]);

        $normalizers = [new ArrayDenormalizer(), new ObjectNormalizer(null, null, null, $propertyTypeExtractor)];
        $encoders    = [new JsonEncoder()];

        return new Serializer($normalizers, $encoders);
    }
}
