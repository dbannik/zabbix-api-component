<?php

declare(strict_types=1);

namespace Zabbix\Middleware;

use Zabbix\Credentials;
use Zabbix\Envelope;
use Zabbix\Exception\UnauthorizedException;
use Zabbix\Handler\RequestHandler;
use Zabbix\Model\AuthorizeRequest;
use Zabbix\Model\AuthResponse;
use Zabbix\Model\Subject\SessionIdentification;

final class AuthorizeMiddleware implements MiddlewareInterface
{
    private Credentials $credentials;

    private RequestHandler $handler;

    public function __construct(Credentials $credentials, RequestHandler $handler)
    {
        $this->credentials = $credentials;
        $this->handler     = $handler;
    }

    public function execute(Envelope $envelope, callable $next): Envelope
    {
        if (null === $envelope->getStorage()->getSession()) {
            $request  = new AuthorizeRequest($this->credentials->getUsername(), $this->credentials->getPassword());
            $response = ($this->handler)($request, AuthResponse::class);

            if ($response->isError()) {
                throw new UnauthorizedException((string) $response->error);
            }

            if ($response instanceof AuthResponse) {
                $envelope->getStorage()->setSession(new SessionIdentification($response->getId(), $response->getResult()));
            }
        }

        return $next($envelope);
    }
}
