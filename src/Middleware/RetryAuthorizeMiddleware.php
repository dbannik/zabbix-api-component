<?php

declare(strict_types=1);

namespace Zabbix\Middleware;

use Zabbix\Envelope;
use Zabbix\Exception\RetryException;

final class RetryAuthorizeMiddleware implements MiddlewareInterface
{
    private int $maxRetryNumber;

    public function __construct(int $maxRetryNumber)
    {
        $this->maxRetryNumber = $maxRetryNumber;
    }

    public function execute(Envelope $envelope, callable $next): Envelope
    {
        $next($envelope);

        $needRetry = static fn (Envelope $envelope) => (
            isset($envelope->modelResponse->error->data)
            && 'Session terminated, re-login, please.' === $envelope->modelResponse->error->data
        );

        for ($retry = 0; $needRetry($envelope); ++$retry) {
            if ($retry >= $this->maxRetryNumber) {
                throw new RetryException(\sprintf('Achieved maximum number of retry %s.', $this->maxRetryNumber));
            }
            $envelope->getStorage()->setSession(null);
            $next($envelope);
        }

        return $envelope;
    }
}
