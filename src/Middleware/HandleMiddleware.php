<?php

declare(strict_types=1);

namespace Zabbix\Middleware;

use Zabbix\Envelope;
use Zabbix\Handler\RequestHandler;

final class HandleMiddleware implements MiddlewareInterface
{
    private RequestHandler $handler;

    public function __construct(RequestHandler $handler)
    {
        $this->handler = $handler;
    }

    public function execute(Envelope $envelope, callable $next): Envelope
    {
        $requestModel  = $envelope->getModelRequest();
        $responseClass = $envelope->getResponseModalClass();

        if (null !== $envelope->getStorage()->getSession()) {
            $requestModel->setSession($envelope->getStorage()->getSession());
        }

        $responseModal           = ($this->handler)($requestModel, $responseClass);
        $envelope->modelResponse = $responseModal;

        return $envelope;
    }
}
