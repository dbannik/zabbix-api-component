<?php

declare(strict_types=1);

namespace Zabbix\Middleware;

use Zabbix\Envelope;

interface MiddlewareInterface
{
    public function execute(Envelope $envelope, callable $next): Envelope;
}
