<?php

declare(strict_types=1);

namespace Tests;

use PHPUnit\Framework\TestCase;
use Zabbix\Client;
use Zabbix\Credentials;
use Zabbix\Factory\ClientFactory;
use Zabbix\Message\AddHostMessage;
use Zabbix\Message\DeleteHostMessage;
use Zabbix\Message\GetGroupMessage;
use Zabbix\Message\GetHostsMessage;
use Zabbix\Message\GetTemplatesMessage;
use Zabbix\Message\UpdateHostMessage;
use Zabbix\Model\HostGroupsResponse;
use Zabbix\Model\HostIdsResponse;
use Zabbix\Model\HostsResponse;
use Zabbix\Model\Subject\Host;
use Zabbix\Model\Subject\HostGroup;
use Zabbix\Model\Subject\Template;
use Zabbix\Model\TemplateResponse;

/**
 * @SuppressWarnings("CouplingBetweenObjects")
 */
final class MiddlewareTest extends TestCase
{
    private static Client $client;

    protected function setUp(): void
    {
        $username = (string) \getenv('ZABBIX_USERNAME');
        $password = (string) \getenv('ZABBIX_PASSWORD');
        $host     = (string) \getenv('ZABBIX_HOST');

        self::$client ?? self::$client = ClientFactory::create($host, new Credentials($username, $password));
    }

    public function testGetGroup(): string
    {
        $message        = new GetGroupMessage();
        $message->names = ['Templates/Applications'];

        /** @var HostGroupsResponse $response */
        $response = self::$client->handle($message);

        self::assertFalse($response->isError(), (string) $response->error);
        self::assertCount(1, $response->getResult());
        self::assertContainsOnlyInstancesOf(HostGroup::class, $response->getResult());
        self::assertInstanceOf(HostGroup::class, \current($response->getResult()));

        return \current($response->getResult())->groupid;
    }

    /**
     * @depends testGetGroup
     *
     * @param string $groupId
     *
     * @return array<null|string>
     */
    public function testGetTemplates(string $groupId): array
    {
        $message        = new GetTemplatesMessage();
        $message->names = ['Apache by Zabbix agent'];

        /** @var TemplateResponse $response */
        $response = self::$client->handle($message);

        self::assertFalse($response->isError(), (string) $response->error);
        self::assertCount(1, $response->getResult());
        self::assertContainsOnlyInstancesOf(Template::class, $response->getResult());
        self::assertInstanceOf(Template::class, \current($response->getResult()));

        return ['group' => $groupId, 'template' => \current($response->getResult())->templateid];
    }

    /**
     * @depends testGetTemplates
     *
     * @param array<null|string> $params
     *
     * @return string
     */
    public function testCreateHost(array $params): string
    {
        $message                       = new AddHostMessage('test' . md5((string) time()));
        $message->interface->type      = '1';
        $message->interface->main      = '1';
        $message->interface->useip     = '1';
        $message->interface->ip        = '127.0.0.1';
        $message->interface->dns       = '';
        $message->interface->port      = '10050';
        $message->group->groupid       = $params['group'];
        $message->template->templateid = $params['template'];

        /** @var HostIdsResponse $response */
        $response = self::$client->handle($message);

        self::assertFalse($response->isError(), (string) $response->error);
        self::assertCount(1, $response->getHostsIds());
        self::assertIsString(\current($response->getHostsIds()));

        return \current($response->getHostsIds());
    }

    /**
     * @depends testCreateHost
     */
    public function testUpdateHost(string $hostId): string
    {
        $message         = new UpdateHostMessage();
        $message->hostId = $hostId;
        $message->status = 1;

        /** @var HostIdsResponse $response */
        $response = self::$client->handle($message);

        self::assertFalse($response->isError(), (string) $response->error);
        self::assertCount(1, $response->getHostsIds());
        self::assertIsString(\current($response->getHostsIds()));

        return \current($response->getHostsIds());
    }

    /**
     * @depends testUpdateHost
     */
    public function testGetHosts(string $hostId): string
    {
        $message  = new GetHostsMessage();
        $response = self::$client->handle($message);

        self::assertFalse($response->isError(), (string) $response->error);
        self::assertInstanceOf(HostsResponse::class, $response);
        self::assertTrue(\count($response->getResult()) >= 1);
        self::assertContainsOnlyInstancesOf(Host::class, $response->getResult());

        return $hostId;
    }

    /**
     * @depends testGetHosts
     */
    public function testDeleteHost(string $hostId): void
    {
        $message          = new DeleteHostMessage();
        $message->hostIds = [$hostId];
        $response         = self::$client->handle($message);

        self::assertFalse($response->isError(), (string) $response->error);
        self::assertInstanceOf(HostIdsResponse::class, $response);
    }
}
