#!/usr/bin/env bash

docker-compose up -d
BINDING_ADDRESS=$(docker-compose port front 8080)
PORT=${BINDING_ADDRESS//*:}
URL="http://127.0.0.1:${PORT}"

echo "Docker container up"
echo "Waiting for initialized ${URL}"
until curl -sf "${URL}" | grep "Sign in" > /dev/null; do echo -n "."; sleep 1; done

ZABBIX_USERNAME=Admin ZABBIX_PASSWORD=zabbix ZABBIX_HOST=${URL} composer test

docker-compose down -v
